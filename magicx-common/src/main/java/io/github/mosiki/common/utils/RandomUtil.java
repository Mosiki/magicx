package io.github.mosiki.common.utils;

import java.util.UUID;

public class RandomUtil {
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().replace("-", "").toLowerCase();
    }
}
