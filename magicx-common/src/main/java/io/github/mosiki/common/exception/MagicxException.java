package io.github.mosiki.common.exception;

import io.github.mosiki.common.enums.ResultEnum;
import lombok.Getter;

@Getter
public class MagicxException extends RuntimeException {
    private static final long serialVersionUID = -7395881258191597420L;

    private Integer code;

    public MagicxException(ResultEnum resultEnum) {
        super(resultEnum.getMessage());
        this.code = resultEnum.getCode();
    }

    public MagicxException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
