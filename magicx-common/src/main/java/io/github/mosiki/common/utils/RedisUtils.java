package io.github.mosiki.common.utils;

import com.alibaba.fastjson.JSON;

/**
 * Redis工具类
 */
public class RedisUtils {
    /**
     * Object转成JSON数据
     */
    public static String toJson(Object object){
        if(object instanceof Integer || object instanceof Long || object instanceof Float ||
                object instanceof Double || object instanceof Boolean || object instanceof String){
            return String.valueOf(object);
        }
        return JSON.toJSONString(object);
    }

    /**
     * JSON数据，转成Object
     */
    public static <T> T fromJson(String json, Class<T> clazz){
        return JSON.parseObject(json, clazz);
    }
}
