package io.github.mosiki.common.utils;

/**
 * 类名称：StringUtils
 * 类描述：字符串工具类
 * 创建人：WeJan
 * 创建时间：2018年06月25日 11:07
 */
public class StringUtils {

    public static boolean strNotNull(String str) {
        return str != null && str.trim().length() != 0 && str.replaceAll("　", "").length() != 0;
    }

    public static boolean strIsNull(String str) {
        return !strNotNull(str);
    }
}
