package io.github.mosiki.common.enums;

import lombok.Getter;

@Getter
public enum ResultEnum {

    // 每一个大类，code值以整百开始 如：账户相关 1-199  系统相关 500-699


    // 账户相关
    ACCOUNTS_REPEAT(1, "账号已存在"),
    ACCOUNTS_ERROR_PASSWORD(2, "账户名称或密码错误"),
    ACCOUNTS_PASSWORD_NULL(3, "密码不能为空"),
    ACCOUNTS_ERROR_CODE(4, "验证码已失效"),

    // 系统相关
    SYSTEM_ERROR(500, "系统异常"),
    SYSTEM_ERROR_SYMBOL(501, "非法字符"),
    SYSTEM_ERROR_ROLE(502, "没有权限"),
    SYSTEM_MENU_NOT_EXIST(503, "菜单不存在"),
    SYSTEM_PARAMS_FAIL(504, "获取参数失败"),

    // 爬虫相关
    TASK_NOT_EXIST(700, "爬虫任务不存在"),
    TASK_NOT_REPEAT_START(701, "爬虫任务已在运行"),
    TASK_FINISHED(702, "爬虫任务已完成"),
    SPIDER_STATUS_ERROR(703, "爬虫状态有误"),
    TASK_STOPPED(704, "爬虫已停止"),
    TASK_START_FAIL(705, "爬虫启动失败"),
    ;
    private Integer code;
    private String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
