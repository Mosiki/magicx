package io.github.mosiki.common.validator;

import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import io.github.mosiki.common.utils.StringUtils;


/**
 * 类名称：Assert
 * 类描述：数据校验
 * 创建人：WeJan
 * 创建时间：2018年06月25日 11:05
 */
public abstract class Assert {

    public static void strIsNull(String str, ResultEnum resultEnum) {
        if (StringUtils.strIsNull(str)) {
            throw new MagicxException(resultEnum);
        }
    }

    public static void objIsNull(Object object, ResultEnum resultEnum) {
        if (object == null) {
            throw new MagicxException(resultEnum);
        }
    }
}
