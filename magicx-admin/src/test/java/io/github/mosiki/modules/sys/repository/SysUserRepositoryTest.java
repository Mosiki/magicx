package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysUser;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysUserRepositoryTest {

    @Autowired
    private SysUserRepository sysUserRepository;

    @Test
    public void register() throws Exception {

        Page<SysUser> all = sysUserRepository.findAll(PageRequest.of(0, 10));
        Assert.assertNotNull(all.getContent().get(0));
    }
}