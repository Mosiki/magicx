$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'job/schedulejob/list',
        datatype: "json",
        colModel: [
            {label: '任务Id', name: 'jobId', index: 'job_id', width: 50, key: true},
            {label: 'bean名称', name: 'beanName', index: 'bean_name', width: 80},
            {label: '方法名', name: 'methodName', index: 'method_name', width: 80},
            {label: '参数', name: 'params', index: 'params', width: 80},
            {label: 'cron表达式', name: 'cronExpression', index: 'cron_expression', width: 80},
            {label: '备注', name: 'remark', index: 'remark', width: 80},
            {
                label: '状态', name: 'status', width: 60, formatter: function (value, options, row) {
                return value === 0 ?
                    '<span class="label label-success">正常</span>' :
                    '<span class="label label-danger">暂停</span>';
            }
            }
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.content",
            page: "page.number" + 1,
            total: "page.totalPages",
            records: "page.totalElements"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

var vm = new Vue({
    el: '#rrapp',
    data: {
        q:{
            beanName: null
        },
        showList: true,
        title: null,
        scheduleJob: {}
    },
    methods: {
        query: function () {
            vm.reload();
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.scheduleJob = {};
        },
        update: function (event) {
            var jobId = getSelectedRow();
            if (jobId == null) {
                return;
            }
            vm.showList = false;
            vm.title = "修改";

            vm.getInfo(jobId)
        },
        saveOrUpdate: function (event) {
            var url = vm.scheduleJob.jobId == null ? "job/schedulejob/save" : "job/schedulejob/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.scheduleJob),
                success: function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        del: function (event) {
            var jobIds = getSelectedRows();
            if (jobIds == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "job/schedulejob/delete",
                    contentType: "application/json",
                    data: JSON.stringify(jobIds),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                $("#jqGrid").trigger("reloadGrid");
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        getInfo: function (jobId) {
            $.get(baseURL + "job/schedulejob/info/" + jobId, function (r) {
                vm.scheduleJob = r.scheduleJob;
                vm.scheduleJob.updateTime = null;
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                page: page
            }).trigger("reloadGrid");
        },
        pause: function (event) {
            var jobIds = getSelectedRows();
            if (jobIds == null) {
                return;
            }

            confirm('确定要暂停选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "job/schedulejob/pause",
                    contentType: "application/json",
                    data: JSON.stringify(jobIds),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                vm.reload();
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        resume: function (event) {
            var jobIds = getSelectedRows();
            if (jobIds == null) {
                return;
            }

            confirm('确定要恢复选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "job/schedulejob/resume",
                    contentType: "application/json",
                    data: JSON.stringify(jobIds),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                vm.reload();
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        runOnce: function (event) {
            var jobIds = getSelectedRows();
            if (jobIds == null) {
                return;
            }

            confirm('确定要立即执行选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "job/schedulejob/run",
                    contentType: "application/json",
                    data: JSON.stringify(jobIds),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                vm.reload();
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                postData: {'beanName': vm.q.beanName},
                page: page
            }).trigger("reloadGrid");
        }
    }
});