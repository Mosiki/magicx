$(function () {
    $("#jqGrid").jqGrid({
        url: baseURL + 'spider/spidertask/list',
        datatype: "json",
        colModel: [
            {label: 'id', name: 'id', index: 'id', width: 50, key: true},
            {label: '任务启动UUID', name: 'spriderUuid', index: 'sprider_uuid', width: 80},
            {label: '站点Id', name: 'siteId', index: 'site_id', width: 80},
            {label: '任务名称', name: 'taskName', index: 'task_name', width: 80},
            {
                label: '运行状态', name: 'runState', width: 60, formatter: function (value, options, row) {
                return value === 'Running' ?
                    '<span class="label label-success">运行中</span>' :
                    '<span class="label label-danger">已停止</span>';
            }
            },
            // { label: '任务规则json', name: 'taskRuleJson', index: 'task_rule_json', width: 80 },
            {
                label: '任务规则', name: 'taskRuleJson', width: 80, formatter: function (value, options, row) {
                return '<span class="label label-success pointer" onclick="vm.showDetail(' + row.id + ')">查看详情</span>';
            }
            },
            {label: '开始时间', name: 'startTime', index: 'start_time', width: 80},
            {label: '结束时间', name: 'endTime', index: 'end_time', width: 80},
            {
                label: '状态', name: 'status', width: 60, formatter: function (value, options, row) {
                return value === 0 ?
                    '<span class="label label-danger">禁用</span>' :
                    '<span class="label label-success">正常</span>';
            }
            },
            {label: '创建时间', name: 'createTime', index: 'create_time', width: 80}
            // { label: '更新时间', name: 'updateTime', index: 'update_time', width: 80 }
        ],
        viewrecords: true,
        height: 385,
        rowNum: 10,
        rowList: [10, 30, 50],
        rownumbers: true,
        rownumWidth: 25,
        autowidth: true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader: {
            root: "page.content",
            page: "page.number" + 1,
            total: "page.totalPages",
            records: "page.totalElements"
        },
        prmNames: {
            page: "page",
            rows: "limit",
            order: "order"
        },
        gridComplete: function () {
            //隐藏grid底部滚动条
            $("#jqGrid").closest(".ui-jqgrid-bdiv").css({"overflow-x": "hidden"});
        }
    });
});

var vm = new Vue({
    el: '#rrapp',
    data: {
        showList: true,
        title: null,
        spiderTask: {}
    },
    methods: {
        query: function () {
            vm.reload();
        },
        add: function () {
            vm.showList = false;
            vm.title = "新增";
            vm.spiderTask = {};
        },
        showDetail: function (id) {
            $.get(baseURL + "spider/spidertask/info/" + id, function (r) {
                parent.layer.open({
                    title: '任务规则json',
                    closeBtn: 0,
                    content: r.spiderTask.taskRuleJson
                });
            });
        },
        update: function (event) {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            vm.showList = false;
            vm.title = "修改";

            vm.getInfo(id)
        },
        saveOrUpdate: function (event) {
            var url = vm.spiderTask.id == null ? "spider/spidertask/save" : "spider/spidertask/update";
            $.ajax({
                type: "POST",
                url: baseURL + url,
                contentType: "application/json",
                data: JSON.stringify(vm.spiderTask),
                success: function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    } else {
                        alert(r.msg);
                    }
                }
            });
        },
        del: function (event) {
            var ids = getSelectedRows();
            if (ids == null) {
                return;
            }

            confirm('确定要删除选中的记录？', function () {
                $.ajax({
                    type: "POST",
                    url: baseURL + "spider/spidertask/delete",
                    contentType: "application/json",
                    data: JSON.stringify(ids),
                    success: function (r) {
                        if (r.code == 0) {
                            alert('操作成功', function (index) {
                                $("#jqGrid").trigger("reloadGrid");
                            });
                        } else {
                            alert(r.msg);
                        }
                    }
                });
            });
        },
        getInfo: function (id) {
            $.get(baseURL + "spider/spidertask/info/" + id, function (r) {
                vm.spiderTask = r.spiderTask;
                vm.spiderTask.updateTime = null;
            });
        },
        reload: function (event) {
            vm.showList = true;
            var page = $("#jqGrid").jqGrid('getGridParam', 'page');
            $("#jqGrid").jqGrid('setGridParam', {
                page: page
            }).trigger("reloadGrid");
        },
        start: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            $.get(baseURL + "spider/spidertask/start/" + id, function (r) {
                if (r.code === 0) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                } else {
                    alert(r.msg);
                }
            });
        },
        startbreak: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            $.get(baseURL + "spider/spidertask/run/break/" + id, function (r) {
                if (r.code === 0) {
                    alert('操作成功', function (index) {
                        vm.reload();
                    });
                } else {
                    alert(r.msg);
                }
            });
        },
        stop: function () {
            var id = getSelectedRow();
            if (id == null) {
                return;
            }
            confirm('确定要停止选中的爬虫任务？', function () {
                $.get(baseURL + "spider/spidertask/stop/" + id, function (r) {
                    if (r.code === 0) {
                        alert('操作成功', function (index) {
                            vm.reload();
                        });
                    } else {
                        alert(r.msg);
                    }
                });
            });
        },
    }
});