package io.github.mosiki.modules.sys.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色
 */
@Data
public class SysRoleDTO implements Serializable {

	private static final long serialVersionUID = 3034283578097511268L;
	/**
	 * 角色ID
	 */
	private Long roleId;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 备注
	 */
	private String remark;

//	/**
//	 * 部门ID
//	 */
//	@NotNull(message="部门不能为空")
//	private Long deptId;

	/**
	 * 部门名称
	 */
//	@TableField(exist=false)
//	private String deptName;
//
	private List<Long> menuIdList;
//	private List<Long> deptIdList;
	
	/**
	 * 创建时间
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

}
