package io.github.mosiki.modules.sys.redis;

import io.github.mosiki.common.utils.RedisKeys;
import io.github.mosiki.modules.sys.domain.SysConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 系统配置Redis
 */


@Component
public class SysConfigRedis {
    @Autowired
    private RedisTemplate redisTemplate;

    public void saveOrUpdate(SysConfig config) {
        if (config == null) {
            return;
        }
        String key = RedisKeys.getSysConfigKey(config.getParamKey());
        redisTemplate.opsForValue().set(key, config);

    }

    public void delete(String configKey) {
        String key = RedisKeys.getSysConfigKey(configKey);
        redisTemplate.opsForValue().getOperations().delete(key);
    }

    public SysConfig get(String configKey) {
        String key = RedisKeys.getSysConfigKey(configKey);
        return (SysConfig) redisTemplate.opsForValue().get(key);
    }
}
