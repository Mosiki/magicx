package io.github.mosiki.modules.sys.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * 登录表单
 */
@Data
public class LoginForm {
//    @Pattern(regexp = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$", message = "邮箱格式不正确")
    @NotEmpty(message = "邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;

    @Length(min = 8, max = 50, message = "密码不正确")
    private String password;

    @NotEmpty(message = "验证码不能为空")
    private String captcha;

    private int age;
}
