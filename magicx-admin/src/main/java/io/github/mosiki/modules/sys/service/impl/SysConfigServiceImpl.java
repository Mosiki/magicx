package io.github.mosiki.modules.sys.service.impl;

import com.google.gson.Gson;
import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.sys.converter.SysConfigConverter;
import io.github.mosiki.modules.sys.domain.SysConfig;
import io.github.mosiki.modules.sys.dto.SysConfigDTO;
import io.github.mosiki.modules.sys.redis.SysConfigRedis;
import io.github.mosiki.modules.sys.repository.SysConfigRepository;
import io.github.mosiki.modules.sys.service.SysConfigService;
import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 类名称：SysConfigServiceImpl
 * 类描述：系统配置信息
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
@Service("sysConfigService")
public class SysConfigServiceImpl implements SysConfigService {
    @Autowired
    private SysConfigRepository sysConfigRepository;
    @Autowired
    private SysConfigRedis sysConfigRedis;

    @Override
    public Page<SysConfigDTO> listConfig(Map<String, Object> params, PageRequest pageRequest) {
        //TODO 参数没用上 动态查询
        Page<SysConfig> all = sysConfigRepository.findAll(pageRequest);
        List<SysConfigDTO> dtos = SysConfigConverter.conver(all.getContent());
        return new PageImpl<>(dtos, pageRequest, all.getTotalElements());
    }

    @Override
    public SysConfigDTO findById(Long id) {
        return SysConfigConverter.conver(sysConfigRepository.findById(id).orElse(null));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysConfig saveConfig(SysConfigDTO sysConfigDTO) {
        sysConfigDTO.setStatus(Constant.STATUS_OK);
        SysConfig sysConfig = sysConfigRepository.save(SysConfigConverter.conver(sysConfigDTO));
        sysConfigRedis.saveOrUpdate(sysConfig);
        return sysConfig;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysConfig updateConfig(SysConfig sysConfig) {
        SysConfig config = sysConfigRepository.save(sysConfig);
        sysConfigRedis.saveOrUpdate(config);
        return config;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteBatchIds(List<Long> ids) {
        for (Long id : ids) {
            SysConfig sysConfig = sysConfigRepository.findById(id).orElse(null);
            sysConfigRedis.delete(sysConfig.getParamKey());
        }
        return sysConfigRepository.deleteByIdIn(ids);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateValueByKey(String key, String value) {
        sysConfigRepository.updateValueByKey(key, value);
        sysConfigRedis.delete(key);
    }

    @Override
    public String getValue(String key) {
        SysConfig config = sysConfigRedis.get(key);
        if(config == null){
            config = sysConfigRepository.findByParamKey(key);
            sysConfigRedis.saveOrUpdate(config);
        }

        return config == null ? null : config.getParamValue();
    }

    @Override
    public <T> T getConfigObject(String key, Class<T> clazz) {
        String value = getValue(key);
        if(StringUtils.isNotBlank(value)){
            return new Gson().fromJson(value, clazz);
        }

        try {
            return clazz.newInstance();
        } catch (Exception e) {
            throw new MagicxException(ResultEnum.SYSTEM_PARAMS_FAIL);
        }
    }
}
