package io.github.mosiki.modules.spider.repository;

import io.github.mosiki.modules.spider.domain.SpiderSite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：SpiderSiteRepository
 * 类描述：爬虫站点
 * 创建人：WeJan
 * 创建时间：2018-07-17 22:08:33
 */
public interface SpiderSiteRepository extends JpaRepository<SpiderSite, Long> {

    @Modifying
    @Query("update SpiderSite set status = 0 where id in ?1")
    int deleteByIdIn(List<Long> ids);
	
}
