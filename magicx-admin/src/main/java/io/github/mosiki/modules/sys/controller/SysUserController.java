package io.github.mosiki.modules.sys.controller;

import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.modules.sys.converter.SysUserConverter;
import io.github.mosiki.modules.sys.domain.SysUser;
import io.github.mosiki.modules.sys.dto.SysUserDTO;
import io.github.mosiki.modules.sys.service.SysUserRoleService;
import io.github.mosiki.modules.sys.service.SysUserService;
import io.github.mosiki.modules.sys.shiro.ShiroUtils;
import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.utils.Result;
import io.github.mosiki.common.validator.Assert;
import org.apache.commons.lang.ArrayUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 系统用户
 */
@RestController
@RequestMapping("/sys/user")
public class SysUserController extends AbstractController {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    /**
     * 所有用户列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:user:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SysUserDTO> result = sysUserService.listUser(params, pageRequest);

        return Result.success().put("page", result);
    }

    /**
     * 获取登录的用户信息
     */
    @RequestMapping("/info")
    public Result info() {
        return Result.success().put("user", getUser());
    }

    /**
     * 修改登录用户密码
     */
    @SystemLog("修改密码")
    @RequestMapping("/password")
    public Result password(String password, String newPassword) {
        Assert.strIsNull(newPassword, ResultEnum.ACCOUNTS_PASSWORD_NULL);

        //原密码
        password = ShiroUtils.sha256(password, getUser().getSalt());
        //新密码
        newPassword = ShiroUtils.sha256(newPassword, getUser().getSalt());

        //更新密码
        boolean flag = sysUserService.updatePassword(getUserId(), password, newPassword);
        if (!flag) {
            return Result.error("原密码不正确");
        }

        return Result.success();
    }

    /**
     * 用户信息
     */
    @RequestMapping("/info/{userId}")
    @RequiresPermissions("sys:user:info")
    public Result info(@PathVariable("userId") Long userId) {
        SysUserDTO user = sysUserService.findById(userId);

        //获取用户所属的角色列表
        List<Long> roleIdList = sysUserRoleService.listRoleId(userId);
        user.setRoleIdList(roleIdList);

        return Result.success().put("user", user);
    }

    /**
     * 保存用户
     */
    @SystemLog("保存用户")
    @RequestMapping("/save")
    @RequiresPermissions("sys:user:save")
    public Result save(@RequestBody @Validated SysUserDTO userDTO) {
        SysUser sysUser = sysUserService.saveUser(userDTO);

        return Result.success();
    }

    /**
     * 修改用户
     */
    @SystemLog("修改用户")
    @RequestMapping("/update")
    @RequiresPermissions("sys:user:update")
    public Result update(@RequestBody SysUserDTO userDTO) {
        sysUserService.updateUser(SysUserConverter.conver(userDTO));

        return Result.success();
    }

    /**
     * 删除用户
     */
    @SystemLog("删除用户")
    @RequestMapping("/delete")
    @RequiresPermissions("sys:user:delete")
    public Result delete(@RequestBody Long[] userIds) {
        if (ArrayUtils.contains(userIds, 1L)) {
            return Result.error("系统管理员不能删除");
        }

        if (ArrayUtils.contains(userIds, getUserId())) {
            return Result.error("当前用户不能删除");
        }

        int count = sysUserService.deleteBatchIds(Arrays.asList(userIds));

        return Result.success().put(count);
    }
}
