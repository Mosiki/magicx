package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class HeadersConfig {
    private String name;

    private String value;

}
