package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class RinseRule {

	private String action;
	private String type;
	private String cssquery;
	private String name;
	private String value;
	private String source;
	private String target;
}