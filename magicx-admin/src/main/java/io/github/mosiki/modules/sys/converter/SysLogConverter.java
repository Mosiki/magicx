package io.github.mosiki.modules.sys.converter;

import io.github.mosiki.modules.sys.domain.SysLog;
import io.github.mosiki.modules.sys.dto.SysLogDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：SysLogConverter
 * 类描述：系统日志
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public class SysLogConverter {

    public static SysLogDTO conver(SysLog sysLog) {
        SysLogDTO sysLogDTO = new SysLogDTO();
        BeanUtils.copyProperties(sysLog, sysLogDTO);
        return sysLogDTO;
    }

    public static SysLog conver(SysLogDTO sysLogDTO) {
        SysLog sysLog = new SysLog();
        BeanUtils.copyProperties(sysLogDTO, sysLog);
        return sysLog;
    }

    public static List<SysLogDTO> conver(List<SysLog> sysLogList) {
        return sysLogList.stream().map(SysLogConverter::conver).collect(Collectors.toList());
    }
}
