package io.github.mosiki.modules.job.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * 类名称：ScheduleJob
 * 类描述：定时任务
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
@Data
@Entity
@DynamicUpdate
public class ScheduleJob implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 任务id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long jobId;
	/**
	 * spring bean名称
	 */
	private String beanName;
	/**
	 * 方法名
	 */
	private String methodName;
	/**
	 * 参数
	 */
	private String params;
	/**
	 * cron表达式
	 */
	private String cronExpression;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 任务状态  0：正常  1：暂停
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 
	 */
	private Date updateTime;
}
