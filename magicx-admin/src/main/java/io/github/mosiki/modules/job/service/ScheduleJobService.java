package io.github.mosiki.modules.job.service;

import io.github.mosiki.modules.job.domain.ScheduleJob;
import io.github.mosiki.modules.job.dto.ScheduleJobDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

/**
 * 类名称：ScheduleJobService
 * 类描述：定时任务
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public interface ScheduleJobService {

    Page<ScheduleJobDTO> listScheduleJob(Map<String, Object> params, PageRequest pageRequest);

    ScheduleJobDTO findById(Long jobId);

    ScheduleJob saveScheduleJob(ScheduleJobDTO scheduleJobDTO);

    ScheduleJob updateScheduleJob(ScheduleJobDTO scheduleJob);

    int deleteBatchIds(List<Long> jobIds);

    int updateBatch(Long[] jobIds, int status);

    void run(Long[] jobIds);

    void pause(Long[] jobIds);

    void resume(Long[] jobIds);
}

