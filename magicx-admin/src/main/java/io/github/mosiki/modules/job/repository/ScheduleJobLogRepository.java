package io.github.mosiki.modules.job.repository;

import io.github.mosiki.modules.job.domain.ScheduleJobLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：ScheduleJobLogRepository
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public interface ScheduleJobLogRepository extends JpaRepository<ScheduleJobLog, Long> {

    @Modifying
    @Query("update ScheduleJobLog set status = 0 where logId in ?1")
    int deleteByLogIdIn(List<Long> logIds);
	
}
