package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class SpiderExceptionMsg {

	private String title;
	private String msg;
	private Integer type;

    public SpiderExceptionMsg(String title, String msg, Integer type) {
        this.title = title;
        this.msg = msg;
        this.type = type;
    }

    public SpiderExceptionMsg() {
    }
}
