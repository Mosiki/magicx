package io.github.mosiki.modules.spider.config;

import lombok.Data;

import java.util.List;

@Data
public class RuleMatchConfig {
	private String listregex;

	private String listxpath;

	private String detailregex;

	private String loadlistxpath;

	private String itemxpath;

	private String itemImagexpath;

	private List<DetailxpathConfig> detailxpath;
}
