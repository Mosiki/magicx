package io.github.mosiki.modules.spider.dto;

import lombok.Data;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 类名称：SpiderTaskDTO
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
@Data
public class SpiderTaskDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private Long id;
    /**
     * 结束时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 运行状态
     */
    private String runState;
    /**
     * 站点Id
     */
    private Long siteId;
    /**
     * 开始时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 任务规则json
     */
    private String taskRuleJson;
    /**
     * 任务启动UUID
     */
    private String spriderUuid;
    /**
     * 需要立即执行任务:是(1);否(2)
     */
//    private String runTask;
    /**
     * 是否是定时任务:是(1);否(2)
     */
//    private String timerTask;
    /**
     * 状态  0：禁用   1：正常
     */
    private Integer status;
    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
