package io.github.mosiki.modules.spider.repository;

import io.github.mosiki.modules.spider.domain.SpiderTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：SpiderTaskRepository
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
public interface SpiderTaskRepository extends JpaRepository<SpiderTask, Long> {

    @Modifying
    @Query("delete from SpiderTask where id in ?1")
    int deleteByIdIn(List<Long> ids);
	
}
