package io.github.mosiki.modules.spider.service;

import io.github.mosiki.modules.spider.domain.SpiderSite;
import io.github.mosiki.modules.spider.dto.SpiderSiteDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

/**
 * 类名称：SpiderSiteService
 * 类描述：爬虫站点
 * 创建人：WeJan
 * 创建时间：2018-07-17 22:08:33
 */
public interface SpiderSiteService {

    Page<SpiderSiteDTO> listSpiderSite(Map<String, Object> params, PageRequest pageRequest);

    SpiderSiteDTO findById(Long id);

    SpiderSite saveSpiderSite(SpiderSiteDTO spiderSiteDTO);

    SpiderSite updateSpiderSite(SpiderSite spiderSite);

    int deleteBatchIds(List<Long> ids);

}

