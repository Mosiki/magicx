package io.github.mosiki.modules.spider.service.impl;


import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.spider.converter.SpiderSiteConverter;
import io.github.mosiki.modules.spider.domain.SpiderSite;
import io.github.mosiki.modules.spider.dto.SpiderSiteDTO;
import io.github.mosiki.modules.spider.repository.SpiderSiteRepository;
import io.github.mosiki.modules.spider.service.SpiderSiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 类名称：SpiderSiteServiceImpl
 * 类描述：爬虫站点
 * 创建人：WeJan
 * 创建时间：2018-07-17 22:08:33
 */
@Service("spiderSiteService")
public class SpiderSiteServiceImpl implements SpiderSiteService {
    @Autowired
    private SpiderSiteRepository spiderSiteRepository;

    @Override
    public Page<SpiderSiteDTO> listSpiderSite(Map<String, Object> params, PageRequest pageRequest) {
        //TODO 参数没用上 动态查询
        Page<SpiderSite> all = spiderSiteRepository.findAll(pageRequest);
        List<SpiderSiteDTO> dtos = SpiderSiteConverter.conver(all.getContent());
        return new PageImpl<>(dtos, pageRequest, all.getTotalElements());
    }

    @Override
    public SpiderSiteDTO findById(Long id) {
        return SpiderSiteConverter.conver(spiderSiteRepository.findById(id).orElse(null));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public SpiderSite saveSpiderSite(SpiderSiteDTO spiderSiteDTO) {
        spiderSiteDTO.setStatus(Constant.STATUS_OK);
        SpiderSite spiderSite = spiderSiteRepository.save(SpiderSiteConverter.conver(spiderSiteDTO));
        return spiderSite;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SpiderSite updateSpiderSite(SpiderSite spiderSite) {
        return spiderSiteRepository.save(spiderSite);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int deleteBatchIds(List<Long> ids) {
        return spiderSiteRepository.deleteByIdIn(ids);
    }
}
