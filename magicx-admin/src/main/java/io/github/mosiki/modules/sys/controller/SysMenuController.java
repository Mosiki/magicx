package io.github.mosiki.modules.sys.controller;

import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import io.github.mosiki.common.utils.Result;
import io.github.mosiki.common.utils.StringUtils;
import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.sys.domain.SysMenu;
import io.github.mosiki.modules.sys.dto.SysMenuDTO;
import io.github.mosiki.modules.sys.service.SysMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统菜单
 */
@RestController
@RequestMapping("/sys/menu")
public class SysMenuController extends AbstractController {
    @Autowired
    private SysMenuService sysMenuService;

    /**
     * 导航菜单
     */
    @RequestMapping("/nav")
    public Result nav() {
        List<SysMenuDTO> menuList = sysMenuService.getUserMenuList(getUserId());
        return Result.success().put(menuList);
    }

    /**
     * 所有菜单列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:menu:list")
    public List<SysMenuDTO> list() {
        List<SysMenuDTO> menuList = sysMenuService.findAll();
        for (SysMenuDTO SysMenu : menuList) {
            SysMenu parentMenuEntity = sysMenuService.findById(SysMenu.getParentId());
            if (parentMenuEntity != null) {
                SysMenu.setParentName(parentMenuEntity.getName());
            }
        }

        return menuList;
    }

    /**
     * 选择菜单(添加、修改菜单)
     */
    @RequestMapping("/select")
    @RequiresPermissions("sys:menu:select")
    public Result select() {
        //查询列表数据
        List<SysMenu> menuList = sysMenuService.queryNotButtonList();

        //添加顶级菜单
        SysMenu root = new SysMenu();
        root.setMenuId(0L);
        root.setName("一级菜单");
        root.setParentId(-1L);
//        root.setOpen(true);
        menuList.add(root);

        return Result.success().put("menuList", menuList);
    }

    /**
     * 菜单信息
     */
    @RequestMapping("/info/{menuId}")
    @RequiresPermissions("sys:menu:info")
    public Result info(@PathVariable("menuId") Long menuId) {
        SysMenu menu = sysMenuService.findById(menuId);
        return Result.success().put("menu", menu);
    }

    /**
     * 保存
     */
    @SystemLog("保存菜单")
    @RequestMapping("/save")
    @RequiresPermissions("sys:menu:save")
    public Result save(@RequestBody SysMenu menu) {
        //数据校验
        verifyForm(menu);

        sysMenuService.saveOrUpdateSysMenu(menu);

        return Result.success();
    }

    /**
     * 修改
     */
    @SystemLog("修改菜单")
    @RequestMapping("/update")
    @RequiresPermissions("sys:menu:update")
    public Result update(@RequestBody SysMenu menu) {
        //数据校验
        verifyForm(menu);

        sysMenuService.saveOrUpdateSysMenu(menu);

        return Result.success();
    }

    /**
     * 删除
     */
    @SystemLog("删除菜单")
    @RequestMapping("/delete")
    @RequiresPermissions("sys:menu:delete")
    public Result delete(long menuId) {
        if (menuId <= 31) {
            return Result.error("系统菜单，不能删除");
        }

        //判断是否有子菜单或按钮
        List<SysMenu> menuList = sysMenuService.queryListParentId(menuId);
        if (menuList.size() > 0) {
            return Result.error("请先删除子菜单或按钮");
        }

        sysMenuService.deleteSysMenu(menuId);

        return Result.success();
    }

    /**
     * 验证参数是否正确
     */
    private void verifyForm(SysMenu menu) {
        if (StringUtils.strIsNull(menu.getName())) {
            throw new MagicxException(ResultEnum.SYSTEM_ERROR.getCode(), "菜单名称不能为空");
        }

        if (menu.getParentId() == null) {
            throw new MagicxException(ResultEnum.SYSTEM_ERROR.getCode(), "上级菜单不能为空");
        }

        //菜单
        if (menu.getType() == Constant.MenuType.MENU.getValue()) {
            if (StringUtils.strIsNull(menu.getUrl())) {
                throw new MagicxException(ResultEnum.SYSTEM_ERROR.getCode(), "菜单URL不能为空");
            }
        }

        //上级菜单类型
        int parentType = Constant.MenuType.CATALOG.getValue();
        if (menu.getParentId() != 0) {
            SysMenu parentMenu = sysMenuService.findById(menu.getParentId());
            parentType = parentMenu.getType();
        }

        //目录、菜单
        if (menu.getType() == Constant.MenuType.CATALOG.getValue() ||
                menu.getType() == Constant.MenuType.MENU.getValue()) {
            if (parentType != Constant.MenuType.CATALOG.getValue()) {
                throw new MagicxException(ResultEnum.SYSTEM_ERROR.getCode(), "上级菜单只能为目录类型");
            }
            return;
        }

        //按钮
        if (menu.getType() == Constant.MenuType.BUTTON.getValue()) {
            if (parentType != Constant.MenuType.MENU.getValue()) {
                throw new MagicxException(ResultEnum.SYSTEM_ERROR.getCode(), "上级菜单只能为菜单类型");
            }
            return;
        }
    }
}
