package io.github.mosiki.modules.spider.converter;

import io.github.mosiki.modules.spider.domain.SpiderTask;
import io.github.mosiki.modules.spider.dto.SpiderTaskDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：SpiderTaskConverter
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
public class SpiderTaskConverter {

    public static SpiderTaskDTO conver(SpiderTask spiderTask) {
        SpiderTaskDTO spiderTaskDTO = new SpiderTaskDTO();
        BeanUtils.copyProperties(spiderTask, spiderTaskDTO);
        return spiderTaskDTO;
    }

    public static SpiderTask conver(SpiderTaskDTO spiderTaskDTO) {
        SpiderTask spiderTask = new SpiderTask();
        BeanUtils.copyProperties(spiderTaskDTO, spiderTask);
        return spiderTask;
    }

    public static List<SpiderTaskDTO> conver(List<SpiderTask> spiderTaskList) {
        return spiderTaskList.stream().map(SpiderTaskConverter::conver).collect(Collectors.toList());
    }
}
