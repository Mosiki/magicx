package io.github.mosiki.modules.sys.controller;


import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import io.github.mosiki.common.utils.Result;
import io.github.mosiki.modules.sys.domain.SysUser;
import io.github.mosiki.modules.sys.form.LoginForm;
import io.github.mosiki.modules.sys.form.RegisterForm;
import io.github.mosiki.modules.sys.service.SysUserService;
import io.github.mosiki.modules.sys.shiro.ShiroUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * 登录相关
 */
@Controller
public class SysLoginController {
	@Autowired
	private Producer producer;
	@Autowired
    private SysUserService userService;
	
	@RequestMapping("captcha.jpg")
	public void captcha(HttpServletResponse response)throws IOException {
        response.setHeader("Cache-Control", "no-store, no-cache");
        response.setContentType("image/jpeg");

        //生成文字验证码
        String text = producer.createText();
        //生成图片验证码
        BufferedImage image = producer.createImage(text);
        //保存到shiro session
        ShiroUtils.setSessionAttribute(Constants.KAPTCHA_SESSION_KEY, text);
        
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(image, "jpg", out);
	}

	@ResponseBody
	@RequestMapping(value = "/sys/register", method = RequestMethod.POST)
	public Result register(@Validated RegisterForm registerForm) {

		String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		if(!registerForm.getCaptcha().equalsIgnoreCase(kaptcha)){
			return Result.error("验证码不正确");
		}

		SysUser user = userService.register(registerForm.getEmail(), registerForm.getPassword());
		return Result.success().put(user);
	}

	/**
	 * 登录
	 */
	@ResponseBody
	@RequestMapping(value = "/sys/login", method = RequestMethod.POST)
	public Result login(@Validated LoginForm loginForm) {

		String kaptcha = ShiroUtils.getKaptcha(Constants.KAPTCHA_SESSION_KEY);
		if(!loginForm.getCaptcha().equalsIgnoreCase(kaptcha)){
			return Result.error("验证码不正确");
		}

		try{
			Subject subject = ShiroUtils.getSubject();
			UsernamePasswordToken token = new UsernamePasswordToken(loginForm.getEmail(), loginForm.getPassword());
			subject.login(token);
		}catch (UnknownAccountException e) {
			return Result.error(e.getMessage());
		}catch (IncorrectCredentialsException e) {
			throw new MagicxException(ResultEnum.ACCOUNTS_ERROR_PASSWORD);
		}catch (LockedAccountException e) {
			return Result.error("账号已被锁定,请联系管理员");
		}catch (AuthenticationException e) {
			return Result.error("账户验证失败");
		}

		return Result.success();
	}
	
	/**
	 * 退出
	 */
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout() {
		ShiroUtils.logout();
		return "redirect:login.html";
	}
	
}
