package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysUserRepository extends JpaRepository<SysUser, Long> {
    SysUser findByEmail(String email);

    @Query("select distinct rm.menuId from SysUserRole ur LEFT JOIN SysRoleMenu rm on ur.roleId = rm.roleId where ur.userId = ?1")
    List<Long> queryAllMenuId(Long userId);

    SysUser findByUserIdAndPassword(Long userId, String password);

    @Modifying
    @Query("update SysUser set status = 0 where userId in ?1")
    int deleteByUserIdIn(List<Long> ids);
}
