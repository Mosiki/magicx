package io.github.mosiki.modules.sys.controller;

import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.modules.sys.domain.SysRole;
import io.github.mosiki.modules.sys.dto.SysRoleDTO;
import io.github.mosiki.modules.sys.service.SysRoleMenuService;
import io.github.mosiki.modules.sys.service.SysRoleService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 角色管理
 */
@RestController
@RequestMapping("/sys/role")
public class SysRoleController extends AbstractController {
    @Autowired
    private SysRoleService sysRoleService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;
//	@Autowired
//	private SysRoleDeptService sysRoleDeptService;

    /**
     * 角色列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:role:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SysRoleDTO> result = sysRoleService.listRole(params, pageRequest);

        return Result.success().put("page", result);
    }

    /**
     * 角色列表
     */
    @RequestMapping("/select")
    @RequiresPermissions("sys:role:select")
    public Result select() {
        List<SysRole> list = sysRoleService.findAll();

        return Result.success().put("list", list);
    }

    /**
     * 角色信息
     */
    @RequestMapping("/info/{roleId}")
    @RequiresPermissions("sys:role:info")
    public Result info(@PathVariable("roleId") Long roleId) {
        SysRoleDTO role = sysRoleService.findById(roleId);

        //查询角色对应的菜单
        List<Long> menuIdList = sysRoleMenuService.listMenuId(roleId);
        role.setMenuIdList(menuIdList);

//		//查询角色对应的部门
//		List<Long> deptIdList = sysRoleDeptService.queryDeptIdList(new Long[]{roleId});
//		role.setDeptIdList(deptIdList);

        return Result.success().put("role", role);
    }

    /**
     * 保存角色
     */
    @SystemLog("保存角色")
    @RequestMapping("/save")
    @RequiresPermissions("sys:role:save")
    public Result save(@RequestBody SysRoleDTO roleDTO) {
        // TODO 缺少校验
//        ValidatorUtils.validateEntity(role);

        SysRole sysRole = sysRoleService.saveOrUpdateSysRole(roleDTO);

        return Result.success();
    }

    /**
     * 修改角色
     */
    @SystemLog("修改角色")
    @RequestMapping("/update")
    @RequiresPermissions("sys:role:update")
    public Result update(@RequestBody SysRoleDTO roleDTO) {
        // TODO 缺少校验
//        ValidatorUtils.validateEntity(role);

        SysRole sysRole = sysRoleService.saveOrUpdateSysRole(roleDTO);

        return Result.success();
    }

    /**
     * 删除角色
     */
    @SystemLog("删除角色")
    @RequestMapping("/delete")
    @RequiresPermissions("sys:role:delete")
    public Result delete(@RequestBody Long[] roleIds) {
        sysRoleService.deleteBatch(roleIds);

        return Result.success();
    }
}
