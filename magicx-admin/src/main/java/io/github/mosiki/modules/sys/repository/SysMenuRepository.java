package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysMenuRepository extends JpaRepository<SysMenu, Long> {

    /**
     * 根据父菜单，查询子菜单
     * @param parentId 父菜单ID
     */
    @Query("SELECT u from SysMenu u where parentId = ?1 order by orderNum asc")
    List<SysMenu> findByParentId(Long parentId);

    /**
     * 获取不包含按钮的菜单列表
     */
    @Query("SELECT u from SysMenu u where type != 2 order by orderNum asc ")
    List<SysMenu> queryNotButtonList();

    @Query("select m.perms from SysUserRole ur LEFT JOIN SysRoleMenu rm on ur.roleId = rm.roleId LEFT JOIN SysMenu m on rm.menuId = m.menuId where ur.userId = ?1")
    List<String> findAllPerms(Long userId);
}
