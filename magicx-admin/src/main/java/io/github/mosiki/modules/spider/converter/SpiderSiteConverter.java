package io.github.mosiki.modules.spider.converter;

import io.github.mosiki.modules.spider.domain.SpiderSite;
import io.github.mosiki.modules.spider.dto.SpiderSiteDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：SpiderSiteConverter
 * 类描述：爬虫站点
 * 创建人：WeJan
 * 创建时间：2018-07-17 22:08:33
 */
public class SpiderSiteConverter {

    public static SpiderSiteDTO conver(SpiderSite spiderSite) {
        SpiderSiteDTO spiderSiteDTO = new SpiderSiteDTO();
        BeanUtils.copyProperties(spiderSite, spiderSiteDTO);
        return spiderSiteDTO;
    }

    public static SpiderSite conver(SpiderSiteDTO spiderSiteDTO) {
        SpiderSite spiderSite = new SpiderSite();
        BeanUtils.copyProperties(spiderSiteDTO, spiderSite);
        return spiderSite;
    }

    public static List<SpiderSiteDTO> conver(List<SpiderSite> spiderSiteList) {
        return spiderSiteList.stream().map(SpiderSiteConverter::conver).collect(Collectors.toList());
    }
}
