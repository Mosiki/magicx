package io.github.mosiki.modules.spider.config;

import lombok.Data;

import java.util.List;

@Data
public class SiteConfig {
	private String userAgent;

	private String domain;

	private int retry;

	private String charset;

	private int sleepTime;

	private Boolean dynamicProxy;;

	private List<ProxyConfig> proxy;

	private List<HeadersConfig> headers;

	private List<CookiesConfig> cookies;
}
