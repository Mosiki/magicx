package io.github.mosiki.modules.spider.domain;

import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;

/**
 * 类名称：SpiderTask
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
@Data
@Entity
@DynamicUpdate
public class SpiderTask implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 运行状态
	 */
//	private String runState;
	/**
	 * 站点Id
	 */
	private Long siteId;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 任务名称
	 */
	private String taskName;
	/**
	 * 任务规则json
	 */
	private String taskRuleJson;
	/**
	 * 任务启动UUID
	 */
	private String spriderUuid;
	/**
	 * 需要立即执行任务:是(1);否(2)
	 */
//	private String runTask;
	/**
	 * 是否是定时任务:是(1);否(2)
	 */
//	private String timerTask;
	/**
	 * 状态  0：禁用   1：正常
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date createTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
}
