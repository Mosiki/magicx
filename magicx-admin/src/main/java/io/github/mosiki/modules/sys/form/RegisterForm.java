package io.github.mosiki.modules.sys.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 注册表单
 */
@Data
public class RegisterForm {
    @Pattern(regexp = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$", message = "邮箱格式不正确")
    private String email;

    @Length(min = 8, max = 50, message = "密码长度不合法，最少8位")
    private String password;

    @NotEmpty(message = "验证码不能为空")
    String captcha;
}
