package io.github.mosiki.modules.sys.controller;

import io.github.mosiki.modules.sys.converter.SysConfigConverter;
import io.github.mosiki.modules.sys.domain.SysConfig;
import io.github.mosiki.modules.sys.dto.SysConfigDTO;
import io.github.mosiki.modules.sys.service.SysConfigService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 类名称：SysConfigController
 * 类描述：系统配置信息表
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
@RestController
@RequestMapping("sys/config")
public class SysConfigController {
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:config:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit){
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SysConfigDTO> result = sysConfigService.listConfig(params, pageRequest);
        return Result.success().put("page", result);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("sys:config:info")
    public Result info(@PathVariable("id") Long id){
        SysConfigDTO sysConfigDto = sysConfigService.findById(id);
        return Result.success().put("config", sysConfigDto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("sys:config:save")
    public Result save(@RequestBody @Validated SysConfigDTO sysConfigDTO){
        SysConfig sysConfig = sysConfigService.saveConfig(sysConfigDTO);
        return Result.success();

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("sys:config:update")
    public Result update(@RequestBody SysConfigDTO sysConfigDTO){
        sysConfigService.updateConfig(SysConfigConverter.conver(sysConfigDTO));//全部更新
        return Result.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("sys:config:delete")
    public Result delete(@RequestBody Long[] ids){
        sysConfigService.deleteBatchIds(Arrays.asList(ids));
        return Result.success();
    }

}
