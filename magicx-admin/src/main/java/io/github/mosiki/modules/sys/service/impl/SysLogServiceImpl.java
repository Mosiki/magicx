package io.github.mosiki.modules.sys.service.impl;

import io.github.mosiki.modules.sys.converter.SysLogConverter;
import io.github.mosiki.modules.sys.domain.SysLog;
import io.github.mosiki.modules.sys.dto.SysLogDTO;
import io.github.mosiki.modules.sys.repository.SysLogRepository;
import io.github.mosiki.modules.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 类名称：SysLogServiceImpl
 * 类描述：系统日志
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    private SysLogRepository sysLogRepository;

    @Override
    public Page<SysLogDTO> listLog(Map<String, Object> params, PageRequest pageRequest) {
        //TODO 参数没用上 动态查询
//        return sysLogRepository.findAll(pageRequest);
        Page<SysLog> all = sysLogRepository.findAll(pageRequest);
        List<SysLogDTO> dtos = SysLogConverter.conver(all.getContent());
        return new PageImpl<>(dtos, pageRequest, all.getTotalElements());
    }

    @Override
    public SysLogDTO findById(Long id) {
        return SysLogConverter.conver(sysLogRepository.findById(id).orElse(null));
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public SysLog saveLog(SysLog sysLog) {
        sysLog = sysLogRepository.save(sysLog);
        return sysLog;
    }

}
