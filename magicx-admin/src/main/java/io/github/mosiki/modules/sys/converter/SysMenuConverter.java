package io.github.mosiki.modules.sys.converter;

import io.github.mosiki.modules.sys.domain.SysMenu;
import io.github.mosiki.modules.sys.dto.SysMenuDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SysMenuConverter {

    public static SysMenuDTO conver(SysMenu sysMenu) {
        SysMenuDTO orderDTO = new SysMenuDTO();
        BeanUtils.copyProperties(sysMenu, orderDTO);
        return orderDTO;
    }

    public static List<SysMenuDTO> conver(List<SysMenu> menuList) {
        return menuList.stream().map(SysMenuConverter::conver).collect(Collectors.toList());
    }

}
