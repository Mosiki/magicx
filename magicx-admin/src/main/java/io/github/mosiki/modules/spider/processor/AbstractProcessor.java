package io.github.mosiki.modules.spider.processor;

import io.github.mosiki.modules.spider.config.*;
import lombok.Data;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.List;

@Data
public abstract class AbstractProcessor implements PageProcessor {

    protected String uuid;
    private WebMagicConfig config;
    private List<RinseRule> commRinseRules;

    public abstract void init(WebMagicConfig config);

    @Override
    public Site getSite() {
        SiteConfig siteConfig = this.config.getSite();
        Site site = Site.me().setDomain(siteConfig.getDomain()).setTimeOut(5000).setSleepTime(siteConfig.getSleepTime())
                .setUserAgent(siteConfig.getUserAgent()).setCycleRetryTimes(siteConfig.getRetry()).setRetryTimes(siteConfig.getRetry())
                .setRetrySleepTime(siteConfig.getSleepTime());

        if (siteConfig.getCharset() != null) {
            site.setCharset(siteConfig.getCharset());
        }

        List<HeadersConfig> headersConfigList = siteConfig.getHeaders();
        if (headersConfigList != null) {
            for (HeadersConfig header : headersConfigList) {
                site.addHeader(header.getName(), header.getValue());
            }
        }
        List<CookiesConfig> cookiesConfigs = siteConfig.getCookies();
        if (cookiesConfigs != null) {
            for (CookiesConfig cookie : cookiesConfigs) {
                site.addCookie(cookie.getName(), cookie.getValue());
            }
        }

        /*List<ProxyConfig> proxyConfigs = siteConfig.getProxy();
        if (proxyConfigs != null && proxyConfigs.size() == 1) {
            ProxyConfig proxyConfig = new ProxyConfig();
            site.setHttpProxy(new HttpHost(proxyConfig.getAddress(), proxyConfig.getProt()));
            // 设置代理的认证
            if (StringUtils.isNotBlank(proxyConfig.getUsername()) && StringUtils.isNotBlank(proxyConfig.getPassword())) {
                site.setUsernamePasswordCredentials(new UsernamePasswordCredentials(proxyConfig.getUsername(), proxyConfig.getPassword()));
            }

        } else if (proxyConfigs != null && proxyConfigs.size() > 1) {
            List<String[]> proxyList = new ArrayList<String[]>();
            for (ProxyConfig proxyConfig : proxyConfigs) {
                proxyList.add(new String[] { proxyConfig.getName(), proxyConfig.getPassword(), proxyConfig.getAddress(), String.valueOf(proxyConfig.getProt()) });
            }
            site.setHttpProxyPool(proxyList, false);
        }*/
        return site;
    }
}
