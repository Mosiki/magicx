package io.github.mosiki.modules.sys.service.impl;

import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.sys.converter.SysMenuConverter;
import io.github.mosiki.modules.sys.domain.SysMenu;
import io.github.mosiki.modules.sys.dto.SysMenuDTO;
import io.github.mosiki.modules.sys.repository.SysMenuRepository;
import io.github.mosiki.modules.sys.service.SysMenuService;
import io.github.mosiki.modules.sys.service.SysRoleMenuService;
import io.github.mosiki.modules.sys.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("sysMenuService")
public class SysMenuServiceImpl implements SysMenuService {

    @Autowired
    private SysMenuRepository sysMenuRepository;
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @Override
    public List<SysMenuDTO> getUserMenuList(Long userId) {
        //系统管理员，拥有最高权限
        if (userId == Constant.SUPER_ADMIN) {
            return getAllMenuList(null);
        }

        //用户菜单列表
        List<Long> menuIdList = sysUserService.queryAllMenuId(userId);
        return getAllMenuList(menuIdList);
    }

    @Override
    public List<SysMenuDTO> queryListParentId(Long parentId, List<Long> menuIdList) {
        List<SysMenu> menuList = queryListParentId(parentId);
        if (menuIdList == null) {
            return SysMenuConverter.conver(menuList);
        }

        List<SysMenuDTO> userMenuList = new ArrayList<>();
        for (SysMenu menu : menuList) {
            if (menuIdList.contains(menu.getMenuId())) {
                userMenuList.add(SysMenuConverter.conver(menu));
            }
        }
        return userMenuList;
    }

    @Override
    public List<SysMenu> queryListParentId(Long parentId) {
        return sysMenuRepository.findByParentId(parentId);
    }

    /**
     * 获取所有菜单列表
     */
    private List<SysMenuDTO> getAllMenuList(List<Long> menuIdList) {
        //查询根菜单列表
        List<SysMenuDTO> menuList = queryListParentId(0L, menuIdList);
        //递归获取子菜单
        getMenuTreeList(menuList, menuIdList);

        return menuList;
    }

    /**
     * 递归
     */
    private List<SysMenuDTO> getMenuTreeList(List<SysMenuDTO> menuList, List<Long> menuIdList) {
        List<SysMenuDTO> subMenuList = new ArrayList<>();

        for (SysMenuDTO entity : menuList) {
            //目录
            if (entity.getType() == Constant.MenuType.CATALOG.getValue()) {
                entity.setList(getMenuTreeList(queryListParentId(entity.getMenuId(), menuIdList), menuIdList));
            }
            subMenuList.add(entity);
        }

        return subMenuList;
    }

    @Override
    public List<SysMenuDTO> findAll() {
        return SysMenuConverter.conver(sysMenuRepository.findAll());
    }

    @Override
    public SysMenu findById(Long id) {
        Optional<SysMenu> sysMenu = sysMenuRepository.findById(id);
        // id 为 0时是不存在，返回 Null
        return sysMenu.orElse(null);
    }

    @Override
    public List<SysMenu> queryNotButtonList() {
        return sysMenuRepository.queryNotButtonList();
    }

    @Override
    public SysMenu saveOrUpdateSysMenu(SysMenu menu) {
        return sysMenuRepository.save(menu);
    }

    @Override
    public int deleteSysMenu(long menuId) {
        SysMenu sysMenu = this.findById(menuId);
        if (sysMenu == null) {
            throw new MagicxException(ResultEnum.SYSTEM_MENU_NOT_EXIST);
        }
        sysMenuRepository.delete(sysMenu);

        int count = sysRoleMenuService.deleteBatchByMenuId(menuId);

        return count;
    }
}
