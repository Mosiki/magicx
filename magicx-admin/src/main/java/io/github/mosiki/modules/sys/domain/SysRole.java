package io.github.mosiki.modules.sys.domain;


import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * 角色
 */
@Data
@Entity
@DynamicUpdate
public class SysRole implements Serializable {

	private static final long serialVersionUID = 4265262610188997708L;
	/**
	 * 角色ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long roleId;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 备注
	 */
	private String remark;

//	/**
//	 * 部门ID
//	 */
//	@NotNull(message="部门不能为空")
//	private Long deptId;

	/**
	 * 部门名称
	 */
//	@TableField(exist=false)
//	private String deptName;
//
//	@TableField(exist=false)
//	private List<Long> menuIdList;
//	@TableField(exist=false)
//	private List<Long> deptIdList;
	
	/**
	 * 创建时间
	 */
	private Date createTime;


	private Date updateTime;

}
