package io.github.mosiki.modules.job.service;

import io.github.mosiki.modules.job.domain.ScheduleJobLog;
import io.github.mosiki.modules.job.dto.ScheduleJobLogDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Map;

/**
 * 类名称：ScheduleJobLogService
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public interface ScheduleJobLogService {

    Page<ScheduleJobLogDTO> listScheduleJobLog(Map<String, Object> params, PageRequest pageRequest);

    ScheduleJobLogDTO findById(Long logId);

    ScheduleJobLog saveScheduleJobLog(ScheduleJobLogDTO scheduleJobLogDTO);

}

