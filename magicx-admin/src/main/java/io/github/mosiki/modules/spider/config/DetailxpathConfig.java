package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class DetailxpathConfig {
	private String name;

	private String reg;

	private String value;

	private String simpleDateFormat;

	public DetailxpathConfig(String name, String reg, String value) {
		this.name = name;
		this.reg = reg;
		this.value = value;
	}

	public DetailxpathConfig(String name,  String value) {
		this.name = name;
		this.value = value;
	}
	public DetailxpathConfig() {
	}
}
