package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class WechatConfig {
	private String name;

	private String uid;

	private String url;
}
