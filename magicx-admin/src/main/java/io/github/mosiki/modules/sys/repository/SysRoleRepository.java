package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface SysRoleRepository extends JpaRepository<SysRole, Long>{

    @Modifying
    @Query("delete from SysRole where roleId in ?1")
    int deleteByRoleId(Long[] roleIds);
}
