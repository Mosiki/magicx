package io.github.mosiki.modules.sys.service;

import io.github.mosiki.modules.sys.domain.SysMenu;
import io.github.mosiki.modules.sys.dto.SysMenuDTO;

import java.util.List;

public interface SysMenuService {
    List<SysMenuDTO> getUserMenuList(Long userId);

    List<SysMenuDTO> queryListParentId(Long parentId, List<Long> menuIdList);

    List<SysMenu> queryListParentId(Long parentId);

    List<SysMenuDTO> findAll();

    SysMenu findById(Long id);

    List<SysMenu> queryNotButtonList();

    SysMenu saveOrUpdateSysMenu(SysMenu menu);

    int deleteSysMenu(long menuId);
}
