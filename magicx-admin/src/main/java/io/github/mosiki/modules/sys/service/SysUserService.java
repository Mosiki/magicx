package io.github.mosiki.modules.sys.service;

import io.github.mosiki.modules.sys.domain.SysUser;
import io.github.mosiki.modules.sys.dto.SysUserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 */
public interface SysUserService {

    SysUser register(String email, String password);

    List<Long> queryAllMenuId(Long userId);

    Page<SysUserDTO> listUser(Map<String, Object> params, PageRequest pageRequest);

    /**
     * 修改密码
     * @param userId
     * @param password
     * @param newPassword
     * @return
     */
    boolean updatePassword(Long userId, String password, String newPassword);

    SysUserDTO findById(Long userId);

    SysUser updateUser(SysUser user);

    int deleteBatchIds(List<Long> ids);

    SysUser saveUser(SysUserDTO userDTO);


//	PageUtils queryPage(Map<String, Object> params);
//
//	/**
//	 * 查询用户的所有菜单ID
//	 */
//	List<Long> queryAllMenuId(Long userId);
//
//	/**
//	 * 保存用户
//	 */
//	void saveOrUpdateSysMenu(SysUserEntity user);
//
//	/**
//	 * 修改用户
//	 */
//	void update(SysUserEntity user);
//
//	/**
//	 * 修改密码
//	 * @param userId       用户ID
//	 * @param password     原密码
//	 * @param newPassword  新密码
//	 */
//	boolean updatePassword(Long userId, String password, String newPassword);

}
