package io.github.mosiki.modules.sys.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SysRoleMenuService {
    List<Long> listMenuId(Long roleId);

    void saveOrUpdate(Long roleId, List<Long> menuIdList);

    @Transactional
    void deleteBatchByRoleId(Long[] ids);

    @Transactional
    int deleteBatchByMenuId(long menuId);
}
