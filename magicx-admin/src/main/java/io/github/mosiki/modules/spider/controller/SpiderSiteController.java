package io.github.mosiki.modules.spider.controller;

import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.modules.spider.converter.SpiderSiteConverter;
import io.github.mosiki.modules.spider.domain.SpiderSite;
import io.github.mosiki.modules.spider.dto.SpiderSiteDTO;
import io.github.mosiki.modules.spider.service.SpiderSiteService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;




/**
 * 类名称：SpiderSiteController
 * 类描述：爬虫站点
 * 创建人：WeJan
 * 创建时间：2018-07-17 22:08:33
 */
@RestController
@RequestMapping("spider/spidersite")
public class SpiderSiteController {
    @Autowired
    private SpiderSiteService spiderSiteService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("spider:spidersite:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit){
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SpiderSiteDTO> result = spiderSiteService.listSpiderSite(params, pageRequest);
        return Result.success().put("page", result);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("spider:spidersite:info")
    public Result info(@PathVariable("id") Long id){
        SpiderSiteDTO spiderSiteDto = spiderSiteService.findById(id);
        return Result.success().put("spiderSite", spiderSiteDto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @SystemLog("新增爬虫站点")
    @RequiresPermissions("spider:spidersite:save")
    public Result save(@RequestBody @Validated SpiderSiteDTO spiderSiteDTO){
        SpiderSite spiderSite = spiderSiteService.saveSpiderSite(spiderSiteDTO);
        return Result.success();

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @SystemLog("修改爬虫站点")
    @RequiresPermissions("spider:spidersite:update")
    public Result update(@RequestBody SpiderSiteDTO spiderSiteDTO){
        spiderSiteService.updateSpiderSite(SpiderSiteConverter.conver(spiderSiteDTO));//全部更新
        return Result.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @SystemLog("删除爬虫站点")
    @RequiresPermissions("spider:spidersite:delete")
    public Result delete(@RequestBody Long[] ids){
        spiderSiteService.deleteBatchIds(Arrays.asList(ids));
        return Result.success();
    }

}
