package io.github.mosiki.modules.spider.config;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class WebMagicConfig {
	private SiteConfig site;

	private SpiderConfig spider;

	private Boolean isCircle;

	private Long circleInterval;

	private Map condition;

	private WechatConfig wechat;

	private RuleMatchConfig rule;

	private List<RinseRule> rinseRules;
}
