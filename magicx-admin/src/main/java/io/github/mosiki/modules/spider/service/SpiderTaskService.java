package io.github.mosiki.modules.spider.service;

import io.github.mosiki.modules.spider.domain.SpiderTask;
import io.github.mosiki.modules.spider.dto.SpiderTaskDTO;
import io.github.mosiki.modules.spider.monitor.MySpiderStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 类名称：SpiderTaskService
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
public interface SpiderTaskService {

    Page<SpiderTaskDTO> listSpiderTask(Map<String, Object> params, PageRequest pageRequest);

    SpiderTaskDTO findById(Long id);

    SpiderTask saveSpiderTask(SpiderTaskDTO spiderTaskDTO);

    SpiderTask updateSpiderTask(SpiderTask spiderTask);

    int deleteBatchIds(List<Long> ids);

    @Transactional(rollbackFor = Exception.class)
    void startTask(String taskId);

    @Transactional(rollbackFor = Exception.class)
    void stopTask(String taskId);

    /**
     * 启动断点抓取/分布式抓取
     * @param taskId
     */
    @Transactional(rollbackFor = Exception.class)
    void startBreakTask(String taskId);

    /**
     * 设置爬虫运行状态
     * @param spiderTaskDTO
     */
    void setSpiderTaskRunState(SpiderTaskDTO spiderTaskDTO);

    /**
     * 获取已注册的Spider
     * @param spiderTaskDTO
     */
    MySpiderStatus getSpiderTaskInstance(SpiderTaskDTO spiderTaskDTO);
}

