package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysUserRoleRepository extends JpaRepository<SysUserRole, Long> {

    @Query("select roleId from SysUserRole where userId = ?1")
    List<Long> listRoleId(Long userId);

    @Modifying
    @Query("delete from SysUserRole where userId = ?1")
    int deleteByUserId(Long userId);

    @Modifying
    @Query("delete from SysUserRole where roleId in ?1")
    int deleteByRoleId(Long[] roleIds);
}
