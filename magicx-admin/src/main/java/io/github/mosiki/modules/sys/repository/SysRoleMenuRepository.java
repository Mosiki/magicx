package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysRoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysRoleMenuRepository extends JpaRepository<SysRoleMenu, Long>{

    @Query("select menuId from SysRoleMenu where roleId = ?1")
    List<Long> listMenuId(Long roleId);

    @Modifying
    @Query("delete from SysRoleMenu where roleId in ?1")
    int deleteBatchByRoleId(Long[] ids);

    @Modifying
    @Query("delete from SysRoleMenu where menuId = ?1")
    int deleteBatchByMenuId(long menuId);
}
