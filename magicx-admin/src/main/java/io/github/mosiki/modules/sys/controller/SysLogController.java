package io.github.mosiki.modules.sys.controller;

import io.github.mosiki.modules.sys.dto.SysLogDTO;
import io.github.mosiki.modules.sys.service.SysLogService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * 类名称：SysLogController
 * 类描述：系统日志
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
@RestController
@RequestMapping("sys/log")
public class SysLogController {
    @Autowired
    private SysLogService sysLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("sys:log:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit){
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SysLogDTO> result = sysLogService.listLog(params, pageRequest);
        return Result.success().put("page", result);
    }

}
