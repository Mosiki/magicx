package io.github.mosiki.modules.sys.service.impl;


import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.sys.converter.SysUserConverter;
import io.github.mosiki.modules.sys.domain.SysUser;
import io.github.mosiki.modules.sys.dto.SysUserDTO;
import io.github.mosiki.modules.sys.repository.SysUserRepository;
import io.github.mosiki.modules.sys.service.SysUserRoleService;
import io.github.mosiki.modules.sys.service.SysUserService;
import io.github.mosiki.modules.sys.shiro.ShiroUtils;
import io.github.mosiki.common.enums.ResultEnum;
import io.github.mosiki.common.exception.MagicxException;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 系统用户
 */

@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    private SysUserRepository sysUserRepository;
    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Override
    public SysUser register(String email, String password) {

        SysUser user = sysUserRepository.findByEmail(email);

        if (user != null) {
            throw new MagicxException(ResultEnum.ACCOUNTS_REPEAT);
        }
        user = new SysUser();
        user.setEmail(email);
        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        user.setSalt(salt);
        user.setStatus(Constant.STATUS_OK);
        user.setPassword(ShiroUtils.sha256(password, salt));
        return sysUserRepository.save(user);
    }

    @Override
    public List<Long> queryAllMenuId(Long userId) {
        return sysUserRepository.queryAllMenuId(userId);
    }

    @Override
    public Page<SysUserDTO> listUser(Map<String, Object> params, PageRequest pageRequest) {
        //TODO 参数没用上 动态查询
        Page<SysUser> all = sysUserRepository.findAll(pageRequest);
        List<SysUserDTO> dtos = SysUserConverter.conver(all.getContent());
        return new PageImpl<>(dtos, pageRequest, all.getTotalElements());
    }

    @Override
    public boolean updatePassword(Long userId, String password, String newPassword) {
        SysUser user = sysUserRepository.findByUserIdAndPassword(userId, password);

        if (user == null) {
            return true;
        }
        user.setPassword(newPassword);
        sysUserRepository.save(user);
        return true;
    }

    @Override
    public SysUserDTO findById(Long userId) {
        return SysUserConverter.conver(sysUserRepository.findById(userId).orElse(null));
    }


    @Override
    @Transactional
    public SysUser saveUser(SysUserDTO userDTO) {
        //sha256加密
        String salt = RandomStringUtils.randomAlphanumeric(20);
        userDTO.setSalt(salt);
        userDTO.setPassword(ShiroUtils.sha256(userDTO.getPassword(), userDTO.getSalt()));
        SysUser sysUser = sysUserRepository.save(SysUserConverter.conver(userDTO));

        //保存用户与角色关系
        sysUserRoleService.saveOrUpdate(sysUser.getUserId(), userDTO.getRoleIdList());
        return sysUser;
    }

    @Override
    public SysUser updateUser(SysUser user) {
        if(!StringUtils.isBlank(user.getPassword())){
            user.setPassword(ShiroUtils.sha256(user.getPassword(), user.getSalt()));
        }
        return sysUserRepository.save(user);
    }

    @Override
    @Transactional
    public int deleteBatchIds(List<Long> ids) {
        return sysUserRepository.deleteByUserIdIn(ids);
    }

}
