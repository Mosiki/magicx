package io.github.mosiki.modules.sys.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class SysUserDTO implements Serializable {

    private static final long serialVersionUID = 5281597921353872397L;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 邮箱
     */
    @Email
    private String email;

    /**
     * 密码
     */
    @Length(min = 8, message = "密码最小长度为8位")
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 状态  0：禁用   1：正常
     */
    private Integer status;

    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 更新时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 角色ID列表
     */
    private List<Long> roleIdList;
}

