package io.github.mosiki.modules.spider.config;

import lombok.Data;

@Data
public class CookiesConfig {
	private String name;

	private String value;
}
