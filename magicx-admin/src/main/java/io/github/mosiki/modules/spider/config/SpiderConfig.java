package io.github.mosiki.modules.spider.config;

import lombok.Data;

import java.util.List;

@Data
public class SpiderConfig {
	private String siteid;

	private Integer thread;

	private String processer;

	private List<String> pipeline;

	private String downloader;

	private String startUrl;
}
