package io.github.mosiki.modules.job.controller;

import io.github.mosiki.modules.job.dto.ScheduleJobLogDTO;
import io.github.mosiki.modules.job.service.ScheduleJobLogService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;


/**
 * 类名称：ScheduleJobLogController
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
@RestController
@RequestMapping("job/schedulejoblog")
public class ScheduleJobLogController {
    @Autowired
    private ScheduleJobLogService scheduleJobLogService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("job:schedulejoblog:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<ScheduleJobLogDTO> result = scheduleJobLogService.listScheduleJobLog(params, pageRequest);
        return Result.success().put("page", result);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{logId}")
//    @RequiresPermissions("job:schedulejoblog:info")
    public Result info(@PathVariable("logId") Long logId) {
        ScheduleJobLogDTO scheduleJobLogDto = scheduleJobLogService.findById(logId);
        return Result.success().put("scheduleJobLog", scheduleJobLogDto);
    }

}
