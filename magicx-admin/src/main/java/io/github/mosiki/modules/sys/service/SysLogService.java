package io.github.mosiki.modules.sys.service;

import io.github.mosiki.modules.sys.domain.SysLog;
import io.github.mosiki.modules.sys.dto.SysLogDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Map;

/**
 * 类名称：SysLogService
 * 类描述：系统日志
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public interface SysLogService {

    Page<SysLogDTO> listLog(Map<String, Object> params, PageRequest pageRequest);

    SysLogDTO findById(Long id);

    SysLog saveLog(SysLog sysLog);

}

