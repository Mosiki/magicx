package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：SysLogRepository
 * 类描述：系统日志
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public interface SysLogRepository extends JpaRepository<SysLog, Long> {

    @Modifying
    @Query("update SysLog set status = 0 where id in ?1")
    int deleteByIdIn(List<Long> ids);
	
}
