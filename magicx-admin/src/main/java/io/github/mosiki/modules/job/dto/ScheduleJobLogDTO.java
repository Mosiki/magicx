package io.github.mosiki.modules.job.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 类名称：ScheduleJobLogDTO
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
@Data
public class ScheduleJobLogDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务日志id
     */
    private Long logId;
    /**
     * 任务id
     */
    private Long jobId;
    /**
     * spring bean名称
     */
    private String beanName;
    /**
     * 方法名
     */
    private String methodName;
    /**
     * 参数
     */
    private String params;
    /**
     * 失败信息
     */
    private String error;
    /**
     * 耗时(单位：毫秒)
     */
    private Integer times;
    /**
     * 任务状态    0：成功    1：失败
     */
    private Integer status;
    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
