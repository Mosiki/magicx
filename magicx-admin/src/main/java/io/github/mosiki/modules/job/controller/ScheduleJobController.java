package io.github.mosiki.modules.job.controller;

import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.modules.job.converter.ScheduleJobConverter;
import io.github.mosiki.modules.job.domain.ScheduleJob;
import io.github.mosiki.modules.job.dto.ScheduleJobDTO;
import io.github.mosiki.modules.job.service.ScheduleJobService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 类名称：ScheduleJobController
 * 类描述：定时任务
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
@RestController
@RequestMapping("job/schedulejob")
public class ScheduleJobController {
    @Autowired
    private ScheduleJobService scheduleJobService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("job:schedulejob:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<ScheduleJobDTO> result = scheduleJobService.listScheduleJob(params, pageRequest);
        return Result.success().put("page", result);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{jobId}")
    @RequiresPermissions("job:schedulejob:info")
    public Result info(@PathVariable("jobId") Long jobId) {
        ScheduleJobDTO scheduleJobDto = scheduleJobService.findById(jobId);
        return Result.success().put("scheduleJob", scheduleJobDto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @SystemLog("新增定时任务")
    @RequiresPermissions("job:schedulejob:save")
    public Result save(@RequestBody @Validated ScheduleJobDTO scheduleJobDTO) {
        ScheduleJob scheduleJob = scheduleJobService.saveScheduleJob(scheduleJobDTO);
        return Result.success();

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @SystemLog("修改定时任务")
    @RequiresPermissions("job:schedulejob:update")
    public Result update(@RequestBody ScheduleJobDTO scheduleJobDTO) {
        scheduleJobService.updateScheduleJob(scheduleJobDTO);//全部更新
        return Result.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @SystemLog("删除定时任务")
    @RequiresPermissions("job:schedulejob:delete")
    public Result delete(@RequestBody Long[] jobIds) {
        scheduleJobService.deleteBatchIds(Arrays.asList(jobIds));
        return Result.success();
    }

    /**
     * 立即执行任务
     */
    @SystemLog("立即执行任务")
    @RequestMapping("/run")
    @RequiresPermissions("job:schedulejob:run")
    public Result run(@RequestBody Long[] jobIds) {
        scheduleJobService.run(jobIds);

        return Result.success();
    }

    /**
     * 暂停定时任务
     */
    @SystemLog("暂停定时任务")
    @RequestMapping("/pause")
    @RequiresPermissions("job:schedulejob:pause")
    public Result pause(@RequestBody Long[] jobIds) {
        scheduleJobService.pause(jobIds);

        return Result.success();
    }

    /**
     * 恢复定时任务
     */
    @SystemLog("恢复定时任务")
    @RequestMapping("/resume")
    @RequiresPermissions("job:schedulejob:resume")
    public Result resume(@RequestBody Long[] jobIds) {
        scheduleJobService.resume(jobIds);

        return Result.success();
    }
}
