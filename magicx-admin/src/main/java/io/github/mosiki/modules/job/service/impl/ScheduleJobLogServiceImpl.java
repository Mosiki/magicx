package io.github.mosiki.modules.job.service.impl;

import io.github.mosiki.common.utils.Constant;
import io.github.mosiki.modules.job.converter.ScheduleJobLogConverter;
import io.github.mosiki.modules.job.domain.ScheduleJobLog;
import io.github.mosiki.modules.job.dto.ScheduleJobLogDTO;
import io.github.mosiki.modules.job.repository.ScheduleJobLogRepository;
import io.github.mosiki.modules.job.service.ScheduleJobLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * 类名称：ScheduleJobLogServiceImpl
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService {
    @Autowired
    private ScheduleJobLogRepository scheduleJobLogRepository;

    @Override
    public Page<ScheduleJobLogDTO> listScheduleJobLog(Map<String, Object> params, PageRequest pageRequest) {
        //TODO 参数没用上 动态查询
        Page<ScheduleJobLog> all = scheduleJobLogRepository.findAll(pageRequest);
        List<ScheduleJobLogDTO> dtos = ScheduleJobLogConverter.conver(all.getContent());
        return new PageImpl<>(dtos, pageRequest, all.getTotalElements());
    }

    @Override
    public ScheduleJobLogDTO findById(Long logId) {
        return ScheduleJobLogConverter.conver(scheduleJobLogRepository.findById(logId).orElse(null));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ScheduleJobLog saveScheduleJobLog(ScheduleJobLogDTO scheduleJobLogDTO) {
        if (scheduleJobLogDTO.getStatus() == null) {
            scheduleJobLogDTO.setStatus(Constant.STATUS_OK);
        }
        return scheduleJobLogRepository.save(ScheduleJobLogConverter.conver(scheduleJobLogDTO));
    }
}
