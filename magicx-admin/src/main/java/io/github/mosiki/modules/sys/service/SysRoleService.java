package io.github.mosiki.modules.sys.service;

import io.github.mosiki.modules.sys.domain.SysRole;
import io.github.mosiki.modules.sys.dto.SysRoleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

public interface SysRoleService {
    Page<SysRoleDTO> listRole(Map<String, Object> params, PageRequest pageRequest);

    List<SysRole> findAll();

    SysRoleDTO findById(Long roleId);

    SysRole saveOrUpdateSysRole(SysRoleDTO roleDTO);

    void deleteBatch(Long[] roleIds);
}
