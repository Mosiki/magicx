package io.github.mosiki.modules.sys.converter;

import io.github.mosiki.modules.sys.domain.SysUser;
import io.github.mosiki.modules.sys.dto.SysUserDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SysUserConverter {

    public static SysUserDTO conver(SysUser sysUser) {
        SysUserDTO userDTO = new SysUserDTO();
        BeanUtils.copyProperties(sysUser, userDTO);
        return userDTO;
    }

    public static SysUser conver(SysUserDTO sysUserDTO) {
        SysUser sysUser = new SysUser();
        BeanUtils.copyProperties(sysUserDTO, sysUser);
        return sysUser;
    }

    public static List<SysUserDTO> conver(List<SysUser> userList) {
        return userList.stream().map(SysUserConverter::conver).collect(Collectors.toList());
    }
}
