package io.github.mosiki.modules.sys.converter;

import io.github.mosiki.modules.sys.domain.SysRole;
import io.github.mosiki.modules.sys.dto.SysRoleDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class SysRoleConverter {

    public static SysRoleDTO conver(SysRole sysRole) {
        SysRoleDTO sysRoleDTO = new SysRoleDTO();
        BeanUtils.copyProperties(sysRole, sysRoleDTO);
        return sysRoleDTO;
    }

    public static SysRole conver(SysRoleDTO sysRoleDTO) {
        SysRole sysRole = new SysRole();
        BeanUtils.copyProperties(sysRoleDTO, sysRole);
        return sysRole;
    }

    public static List<SysRoleDTO> conver(List<SysRole> sysRoleList) {
        return sysRoleList.stream().map(SysRoleConverter::conver).collect(Collectors.toList());
    }
}
