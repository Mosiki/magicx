package io.github.mosiki.modules.sys.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 类名称：SysConfigDTO
 * 类描述：系统配置信息表
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
@Data
public class SysConfigDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private Long id;
    /**
     * key
     */
    private String paramKey;
    /**
     * value
     */
    private String paramValue;
    /**
     * 状态   0：隐藏   1：显示
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /**
     * 更新时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
