package io.github.mosiki.modules.sys.service;

import java.util.List;

public interface SysUserRoleService {
    /**
     * 获取用户所属的角色列表
     * @param userId
     * @return
     */
    List<Long> listRoleId(Long userId);

    /**
     * 保存用户与角色关系
     * @param userId
     * @param roleIdList
     */
    void saveOrUpdate(Long userId, List<Long> roleIdList);

    /**
     * 根据角色ID数组，批量删除
     */
    void deleteBatch(Long[] roleIds);
}
