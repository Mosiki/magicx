package io.github.mosiki.modules.sys.repository;

import io.github.mosiki.modules.sys.domain.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：SysConfigRepository
 * 类描述：系统配置信息表
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public interface SysConfigRepository extends JpaRepository<SysConfig, Long> {

    @Modifying
    @Query("update SysConfig set status = 0 where id in ?1")
    int deleteByIdIn(List<Long> ids);

    @Modifying
    @Query("update SysConfig set paramValue = ?2 where paramKey = ?1")
    void updateValueByKey(String key, String value);

    SysConfig findByParamKey(String key);
}
