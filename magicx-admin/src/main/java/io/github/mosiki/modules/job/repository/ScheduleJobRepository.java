package io.github.mosiki.modules.job.repository;

import io.github.mosiki.modules.job.domain.ScheduleJob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
/**
 * 类名称：ScheduleJobRepository
 * 类描述：定时任务
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public interface ScheduleJobRepository extends JpaRepository<ScheduleJob, Long> {

    @Modifying
    @Query("delete from ScheduleJob where jobId in ?1")
    int deleteByJobIdIn(List<Long> jobIds);

    @Modifying
    @Query("update ScheduleJob set status = ?2 where jobId in ?1")
    int updateBatch(Long[] jobIds, int status);
}
