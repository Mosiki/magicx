package io.github.mosiki.modules.sys.converter;

import io.github.mosiki.modules.sys.domain.SysConfig;
import io.github.mosiki.modules.sys.dto.SysConfigDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：SysConfigConverter
 * 类描述：系统配置信息表
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public class SysConfigConverter {

    public static SysConfigDTO conver(SysConfig sysConfig) {
        SysConfigDTO sysConfigDTO = new SysConfigDTO();
        BeanUtils.copyProperties(sysConfig, sysConfigDTO);
        return sysConfigDTO;
    }

    public static SysConfig conver(SysConfigDTO sysConfigDTO) {
        SysConfig sysConfig = new SysConfig();
        BeanUtils.copyProperties(sysConfigDTO, sysConfig);
        return sysConfig;
    }

    public static List<SysConfigDTO> conver(List<SysConfig> sysConfigList) {
        return sysConfigList.stream().map(SysConfigConverter::conver).collect(Collectors.toList());
    }
}
