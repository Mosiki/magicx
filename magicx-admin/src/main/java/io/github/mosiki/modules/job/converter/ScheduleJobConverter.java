package io.github.mosiki.modules.job.converter;

import io.github.mosiki.modules.job.domain.ScheduleJob;
import io.github.mosiki.modules.job.dto.ScheduleJobDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：ScheduleJobConverter
 * 类描述：定时任务
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public class ScheduleJobConverter {

    public static ScheduleJobDTO conver(ScheduleJob scheduleJob) {
        ScheduleJobDTO scheduleJobDTO = new ScheduleJobDTO();
        BeanUtils.copyProperties(scheduleJob, scheduleJobDTO);
        return scheduleJobDTO;
    }

    public static ScheduleJob conver(ScheduleJobDTO scheduleJobDTO) {
        ScheduleJob scheduleJob = new ScheduleJob();
        BeanUtils.copyProperties(scheduleJobDTO, scheduleJob);
        return scheduleJob;
    }

    public static List<ScheduleJobDTO> conver(List<ScheduleJob> scheduleJobList) {
        return scheduleJobList.stream().map(ScheduleJobConverter::conver).collect(Collectors.toList());
    }
}
