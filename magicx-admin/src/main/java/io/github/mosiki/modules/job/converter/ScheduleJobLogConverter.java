package io.github.mosiki.modules.job.converter;

import io.github.mosiki.modules.job.domain.ScheduleJobLog;
import io.github.mosiki.modules.job.dto.ScheduleJobLogDTO;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 类名称：ScheduleJobLogConverter
 * 类描述：定时任务日志
 * 创建人：WeJan
 * 创建时间：2018-07-21 13:20:39
 */
public class ScheduleJobLogConverter {

    public static ScheduleJobLogDTO conver(ScheduleJobLog scheduleJobLog) {
        ScheduleJobLogDTO scheduleJobLogDTO = new ScheduleJobLogDTO();
        BeanUtils.copyProperties(scheduleJobLog, scheduleJobLogDTO);
        return scheduleJobLogDTO;
    }

    public static ScheduleJobLog conver(ScheduleJobLogDTO scheduleJobLogDTO) {
        ScheduleJobLog scheduleJobLog = new ScheduleJobLog();
        BeanUtils.copyProperties(scheduleJobLogDTO, scheduleJobLog);
        return scheduleJobLog;
    }

    public static List<ScheduleJobLogDTO> conver(List<ScheduleJobLog> scheduleJobLogList) {
        return scheduleJobLogList.stream().map(ScheduleJobLogConverter::conver).collect(Collectors.toList());
    }
}
