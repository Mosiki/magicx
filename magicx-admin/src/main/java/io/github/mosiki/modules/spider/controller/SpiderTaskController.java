package io.github.mosiki.modules.spider.controller;

import io.github.mosiki.common.annotation.SystemLog;
import io.github.mosiki.modules.spider.converter.SpiderTaskConverter;
import io.github.mosiki.modules.spider.domain.SpiderTask;
import io.github.mosiki.modules.spider.dto.SpiderTaskDTO;
import io.github.mosiki.modules.spider.service.SpiderTaskService;
import io.github.mosiki.common.utils.Result;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;


/**
 * 类名称：SpiderTaskController
 * 类描述：爬虫任务
 * 创建人：WeJan
 * 创建时间：2018-07-17 21:08:08
 */
@RestController
@RequestMapping("spider/spidertask")
public class SpiderTaskController {
    @Autowired
    private SpiderTaskService spiderTaskService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("spider:spidertask:list")
    public Result list(@RequestParam Map<String, Object> params, int page, int limit) {
        PageRequest pageRequest = PageRequest.of(page - 1, limit);
        Page<SpiderTaskDTO> result = spiderTaskService.listSpiderTask(params, pageRequest);
        result.getContent().forEach(e -> spiderTaskService.setSpiderTaskRunState(e));
//        for (SpiderTaskDTO spiderTaskDTO : result.getContent()) {
//            spiderTaskService.setSpiderTaskRunState(spiderTaskDTO);
//        }
        return Result.success().put("page", result);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    @RequiresPermissions("spider:spidertask:info")
    public Result info(@PathVariable("id") Long id) {
        SpiderTaskDTO spiderTaskDto = spiderTaskService.findById(id);
        return Result.success().put("spiderTask", spiderTaskDto);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @SystemLog("新增爬虫任务")
    @RequiresPermissions("spider:spidertask:save")
    public Result save(@RequestBody @Validated SpiderTaskDTO spiderTaskDTO) {
        SpiderTask spiderTask = spiderTaskService.saveSpiderTask(spiderTaskDTO);
        return Result.success();

    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @SystemLog("修改爬虫任务")
    @RequiresPermissions("spider:spidertask:update")
    public Result update(@RequestBody SpiderTaskDTO spiderTaskDTO) {
        spiderTaskService.updateSpiderTask(SpiderTaskConverter.conver(spiderTaskDTO));//全部更新
        return Result.success();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @SystemLog("删除爬虫任务")
    @RequiresPermissions("spider:spidertask:delete")
    public Result delete(@RequestBody Long[] ids) {
        spiderTaskService.deleteBatchIds(Arrays.asList(ids));
        return Result.success();
    }

    @RequestMapping("/start/{id}")
    @SystemLog("开启爬虫任务")
    @RequiresPermissions("spider:spidertask:start")
    public Result start(@PathVariable("id") String taskId) {
        spiderTaskService.startTask(taskId);
        return Result.success();
    }

    @RequestMapping("/stop/{id}")
    @SystemLog("停止爬虫任务")
    @RequiresPermissions("spider:spidertask:stop")
    public Result stop(@PathVariable("id") String taskId) {
        spiderTaskService.stopTask(taskId);
        return Result.success();
    }

    @RequestMapping("/run/break/{id}")
    @SystemLog("开启断点爬虫任务")
    @RequiresPermissions("spider:spidertask:startbreak")
    public Result startBreak(@PathVariable("id") String taskId) {
        spiderTaskService.startBreakTask(taskId);
        return Result.success();
    }
}
