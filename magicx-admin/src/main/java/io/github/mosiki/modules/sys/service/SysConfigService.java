package io.github.mosiki.modules.sys.service;

import io.github.mosiki.modules.sys.domain.SysConfig;
import io.github.mosiki.modules.sys.dto.SysConfigDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Map;

/**
 * 类名称：SysConfigService
 * 类描述：系统配置信息表
 * 创建人：WeJan
 * 创建时间：2018-07-04 16:27:13
 */
public interface SysConfigService {

    Page<SysConfigDTO> listConfig(Map<String, Object> params, PageRequest pageRequest);

    SysConfigDTO findById(Long id);

    SysConfig saveConfig(SysConfigDTO sysConfigDTO);

    SysConfig updateConfig(SysConfig sysConfig);

    int deleteBatchIds(List<Long> ids);

    /**
     * 根据key，更新value
     */
    void updateValueByKey(String key, String value);

    /**
     * 根据key，获取配置的value值
     *
     * @param key           key
     */
    String getValue(String key);

    /**
     * 根据key，获取value的Object对象  反序列化
     * @param key    key
     * @param clazz  Object对象
     */
    <T> T getConfigObject(String key, Class<T> clazz);
}

